package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"

	pb "gitlab.com/gitlab-org/gitaly-proto/go"
	"gitlab.com/gitlab-org/gitaly/auth"
	"gitlab.com/gitlab-org/gitaly/client"

	"google.golang.org/grpc"

	"github.com/gliderlabs/ssh"
)

var sshUserNameKey = struct{}{}
var url = "tcp://localhost:9123"

func uploadPackHandler(s ssh.Session) {
	dialOpts := client.DefaultDialOpts
	if token := os.Getenv("GITALY_TOKEN"); token != "" {
		creds := gitalyauth.RPCCredentials(token)
		dialOpts = append(dialOpts, grpc.WithPerRPCCredentials(creds))
	}

	request := pb.SSHUploadPackRequest{Repository: &pb.Repository{}}

	conn, err := client.Dial(url, dialOpts)
	if err != nil {
		log.Fatalf("dial %q: %v", url, err)
	}
	defer conn.Close()

	ctx, cancel := context.WithCancel(s.Context())
	defer cancel()

	client.UploadPack(ctx, conn, s, s, s.Stderr(), &request)
}

func defaultHandler(s ssh.Session) {
	username := s.Context().Value(sshUserNameKey)

	io.WriteString(s, fmt.Sprintf("Hello %q\n", username))
}

func main() {
	ssh.Handle(func(s ssh.Session) {

		switch s.Command()[0] {
		case "git-upload-pack":
			uploadPackHandler(s)
		default:
			defaultHandler(s)
		}

	})

	publicKeyOption := ssh.PublicKeyAuth(func(ctx ssh.Context, key ssh.PublicKey) bool {
		ctx.SetValue(sshUserNameKey, "andrewn")
		return true // allow all keys, or use ssh.KeysEqual() to compare against known keys
	})

	log.Fatal(ssh.ListenAndServe(":2222", nil, publicKeyOption))
}
